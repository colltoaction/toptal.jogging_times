from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from rest_framework_nested.routers import SimpleRouter, NestedSimpleRouter
from joggingtimes import settings
from server import views

router = SimpleRouter()
router.register(r'users', views.UserViewSet) # created for NSR, but isn't exposed

times_router = NestedSimpleRouter(router, r'users', lookup='user')
times_router.register(r'times', views.TimeViewSet)
times_router.register(r'weeks', views.WeeklyTimeViewSet, base_name = "users")

urlpatterns = patterns('',
    url(r'^api/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/authenticate/', 'rest_framework_jwt.views.obtain_jwt_token'),
    url(r'^api/register/', views.register_user),
    url(r'^api/', include(times_router.urls)),
)

if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += patterns('django.contrib.staticfiles.views',
        url(r'^(?P<path>(?:js|css|img|templates|fonts)/.*)$', 'serve'),
        url(r'', 'serve', {'path': 'base.html'}),
    )