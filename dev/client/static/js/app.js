angular.module('joggingtimes', [
  'ui.bootstrap',
  'ui.router',
  'ngResource',
  'sy.bootstrap.timepicker'
])

.constant('AUTH_EVENTS', {
  loginSuccess: 'auth-login-success',
  loginFailed: 'auth-login-failed',
  logoutSuccess: 'auth-logout-success',
  sessionTimeout: 'auth-session-timeout',
  notAuthenticated: 'auth-not-authenticated',
  notAuthorized: 'auth-not-authorized',
})

.run(function ($rootScope, AUTH_EVENTS, $state, SessionManager) {
  lastAccessedState = null;
  $rootScope.$watch(SessionManager.isAuthenticated, function (val) {
    if (val === true) {
      if (lastAccessedState !== null)
        $state.go(lastAccessedState);
      else
        $state.go("weeks");
    }
    else
      $state.go("login");
  });
  $rootScope.$on('$stateChangeStart', function(e, to) {
    if (to.name.indexOf("log") !== 0 && !SessionManager.isAuthenticated())
    {
      lastAccessedState = to;
      e.preventDefault();
      $state.go("login");
    }
  });

  SessionManager.init();
})

.factory('Times', function ($resource) {
  var Times = $resource("/api/users/:username/times/:id/", {id: '@id', username: '@username'}, {
    'update': { method:'PUT' }
  });
  Times.prototype.__defineGetter__("average_speed", function () {
    return this.distance / moment.duration(this.duration).asHours();
  });
  Times.serialize = function (obj, into) {
    var time = into || new Times();
    angular.extend(time, {
      user: obj.user,
      username: obj.username,
      date: moment(obj.date).format("YYYY-MM-DD"),
      duration: moment(obj.duration).format("HH:mm:ss"),
      distance: obj.distance
    });
    return time;
  };
  Times.prototype.deserialize = function () {
    var d = new Date(1970, 1, 1, 0, 0, 0);
    d.setMilliseconds(moment.duration(this.duration).asMilliseconds());
    return {
      user: this.user,
      username: this.username,
      date: moment(this.date).toDate(),
      duration: d,
      distance: parseFloat(this.distance)
    };
  };
  return Times;
})

.factory('Weeks', function ($resource) {
  var Weeks = $resource("/api/users/:username/weeks/:weekId/", {});
  Weeks.prototype.__defineGetter__("average_speed", function () {
    return this.distance / moment.duration(this.duration).asHours();
  });
  return Weeks;
})

.service('Session', function ($window) {
  this.init = function () {
    if ($window.localStorage.getItem('session.token') !== null)
      this.create($window.localStorage.getItem('session.username'), $window.localStorage.getItem('session.token'));
    else
      this.destroy();
  };
  this.create = function (username, token) {
    $window.localStorage.setItem('session.username', username);
    this.username = username;
    $window.localStorage.setItem('session.token', token);
    this.token = token;
  };
  this.destroy = function () {
    $window.localStorage.removeItem('session.username');
    this.username = null;
    $window.localStorage.removeItem('session.token');
    this.token = null;
  };
  return this;
})

.factory('SessionManager', function ($rootScope, $injector, $location, Session, AUTH_EVENTS) {
  this.init = function () {
    Session.init();
  };
  this.login = function (credentials) {
    $http = $injector.get('$http');
    return $http
      .post('/api/authenticate/', credentials)
      .then(function (res) {
        Session.create(credentials.username, res.data.token);
      });
  };
  this.register = function (credentials) {
    var self = this;
    $http = $injector.get('$http');
    return $http
      .post('/api/register/', credentials)
      .then(function (res) {
        self.login(credentials);
      });
  };
  this.isAuthenticated = function () {
    return !!Session.token;
  };
  this.authorization = function () {
    return 'JWT ' + Session.token;
  };
  this.logout = function () {
    Session.destroy();
  };
  return this;
})

.factory('authInterceptor', function ($q, SessionManager) {
  return {
    request: function (config) {
      config.headers = config.headers || {};
      if (SessionManager.isAuthenticated()) {
        config.headers.Authorization = SessionManager.authorization();
      }
      return config;
    },
    responseError: function (response) {
      if (response.status === 401) {
        SessionManager.notAuthorized();
      }
      else if (response.status === 403) {
        SessionManager.logout();
      }
      return $q.reject(response);
    }
  };
})

.config(function ($httpProvider) {
  $httpProvider.interceptors.push('authInterceptor');
})

.config(function ($resourceProvider) {
  $resourceProvider.defaults.stripTrailingSlashes = false;
})

.config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
  $locationProvider.html5Mode(true);
  $urlRouterProvider.otherwise("/weeks");
  $stateProvider
  .state('weeks', {
    url: "/weeks",
    controller: 'DashboardCtrl',
    templateUrl: '/templates/dashboard.html',
    resolve: {
      username: function (Session) {
        return Session.username;
      },
      weeks: function (Weeks, username) {
        return Weeks.query({ username : username }).$promise;
      }
    }
  })
  .state('weeks.detail', {
    url: "/:week",
    controller: 'WeekCtrl',
    templateUrl: '/templates/week.html',
    resolve: {
      week: function ($stateParams, Weeks, username) {
        return Weeks.query({ weekId: $stateParams.week, username: username }).$promise;
      }
    }
  })
  .state('times', {
    url: "/times?from&to",
    controller: 'TimesCtrl',
    templateUrl: '/templates/times.html',
    resolve: {
      username: function (Session) {
        return Session.username;
      },
      times: function (Times, username, dateFrom, dateTo) {
        return Times.query({ username : username, from: dateFrom, to: dateTo }).$promise;
      },
      dateFrom: function ($stateParams) {
        return $stateParams.from;
      },
      dateTo: function ($stateParams) {
        return $stateParams.to;
      }
    }
  })
  .state('times_detail', {
    url: "/times/:id",
    onEnter: function ($state, $modal, Times, Session, $stateParams) {
      $modal.open({
        controller: 'TimesDetailCtrl',
        templateUrl: '/templates/times_detail.html',
        resolve: {
          username: function () {
            return Session.username;
          },
          time: function () {
            if ($stateParams.id === "new")
              return null;
            return Times.get({ username : Session.username, id : $stateParams.id }).$promise;
          }
        }
      })
      .result['finally'](function(result) {
        return $state.transitionTo("times");
      });
    }
  })
  .state('login', {
    url: '/login',
    controller: 'LoginCtrl',
    templateUrl: '/templates/login.html'
  })
  .state('log_register', {
    url: '/register',
    controller: 'RegisterCtrl',
    templateUrl: '/templates/register.html'
  })
  .state('logout', {
    url: '/logout',
    controller: 'LogoutCtrl',
    templateUrl: '/templates/login.html',
    resolve: {
      message: function (SessionManager) {
        return 'Successfully logged out';
      }
    }
  });
})

.controller('NavbarCtrl', function ($scope, Session, SessionManager) {
  $scope.navCollapsed = true;
  $scope.hasValidSession = false;
  $scope.username = "";

  $scope.$watch(SessionManager.isAuthenticated, function (val) {
    $scope.hasValidSession = val;
    $scope.username = Session.username;
  });
})

.controller('DashboardCtrl', function ($scope, username, weeks) {
  $scope.username = username;
  $scope.weeks = weeks;
})

.controller('WeekCtrl', function ($scope, $parse, username, week) {
  $scope.username = username;
  $scope.hasEntries = week.length > 0;
  var getter = $parse('date');
  $scope.times = _.groupBy(week, function (item) {
    return getter(item);
  });

  $scope.week_distance = _.reduce(week, function(memo, time) {
    return memo + parseFloat(time.distance);
  }, 0);
  var week_duration = _.reduce(week, function(memo, time) {
    return memo.add(moment.duration(time.duration));
  }, moment.duration(0));
  $scope.week_duration = moment.utc(week_duration.asMilliseconds()).format("HH:mm:ss");
  $scope.week_average_speed = $scope.week_distance / week_duration.asHours();
})

.controller('TimesCtrl', function ($scope, $state, username, times, dateFrom, dateTo) {
  $scope.username = username;
  $scope.times = times;

  $scope.dateFrom = dateFrom;
  $scope.dateFromOpened = false;
  $scope.dateFromOpen = function ($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.dateToOpened = false;
    $scope.dateFromOpened = !$scope.dateFromOpened;
  };
  $scope.dateFromChanged = function (dateFrom) {
    $state.go(".", {from: dateFrom && moment(dateFrom).format("YYYY-MM-DD")});
  };
  $scope.dateTo = dateTo;
  $scope.dateToOpened = false;
  $scope.dateToOpen = function ($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.dateFromOpened = false;
    $scope.dateToOpened = !$scope.dateToOpened;
  };
  $scope.dateToChanged = function (dateTo) {
    $state.go(".", {to: dateTo && moment(dateTo).format("YYYY-MM-DD")});
  };
  $scope.deleteTime = function (time, index) {
    time.$remove()
    .then(function () {
      $scope.times.splice(index, 1);
    });
  };
})

.controller('TimesDetailCtrl', function ($scope, $state, $modalInstance, Times, username, time) {
  $scope.editing = !!time;
  $scope.timeEntry = (time && time.deserialize()) || {
    username: username,
    date: new Date(),
    duration: new Date(1970, 1, 1, 1, 0, 0), // one hour
    distance: 0,
    $error: {}
  };

  $scope.username = username;
  $scope.dateOpened = false;
  $scope.dateOpen = function ($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.durationOpened = false;
    $scope.dateOpened = !$scope.dateOpened;
  };
  $scope.durationOpened = false;
  $scope.durationOpen = function ($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.dateOpened = false;
    $scope.durationOpened = !$scope.durationOpened;
  };
  $scope.create = function () {
    Times.serialize($scope.timeEntry)
    .$save()
    .then(function() {
      $modalInstance.close();
    })
    .catch(function(response) {
      $scope.timeEntry.$error = response.data;
    });
  };
  $scope.update = function () {
    Times.serialize($scope.timeEntry, time)
    .$update()
    .then(function() {
      $modalInstance.close();
    })
    .catch(function(response) {
      $scope.timeEntry.$error = response.data;
    });
  };
})

.controller('UsersCtrl', function ($scope, users) {
  $scope.users = users;
})

.controller('LoginCtrl', function ($scope, $state, SessionManager) {
  $scope.user = { username: '', password: '' };
  $scope.message = null;
  $scope.invalidCredentials = false;

  $scope.reload = function() {
    $scope.user.password = '';
    $scope.invalidCredentials = false;
  };
  $scope.submit = function () {
    SessionManager.login($scope.user)
    .then(function () {
      $scope.message = 'Welcome';
    })
    .catch(function () {
      $scope.invalidCredentials = true;
    });
  };
  $scope.register = function() {
    $state.transitionTo('log_register');
  };
})

.controller('RegisterCtrl', function ($scope, SessionManager) {
  $scope.user = {
    username: '',
    password: '' ,
    $error: {}
  };

  $scope.submit = function () {
    SessionManager.register($scope.user)
    .catch(function (response) {
      $scope.user.$error = response.data;
    });
  };
})

.controller('LogoutCtrl', function ($scope, SessionManager, message) {
  SessionManager.logout();
  $scope.message = message;
});
