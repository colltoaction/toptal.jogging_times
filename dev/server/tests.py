from datetime import date, datetime
from decimal import Decimal
from unittest.mock import patch
from django.conf import settings
from django.contrib.auth.models import User
from django.test.utils import override_settings
from rest_framework import status
from rest_framework.test import APITestCase
from server.models import Time

NEW_TEST_USER = 'sjdf63j'
NEW_TEST_PASS = 'kdme34x'

class SpoofDate(date):
    @classmethod
    def today(cls, *args, **kwargs):
        return date(2014, 7, 7)


class SpoofDateTime(datetime):
    @classmethod
    def utcnow(cls, *args, **kwargs):
        print("asdas")
        return datetime(2014, 7, 7, 1, 2, 3)


class WeeksTestCase(APITestCase):
    def setUp(self):
        user = User.objects.create_user(
            NEW_TEST_USER,
            '',
            NEW_TEST_PASS,
        )
        self.client.force_authenticate(user=user)

    def test_weeks_for_new_user(self):
        """A new user returns an empty list of weeks"""
        response = self.client.get('/api/users/sjdf63j/weeks/')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, [])

    @patch('server.views.date', SpoofDate)
    def test_weeks_for_user_with_one_entry(self):
        """Creating a new entry for today and then weeks return this monday"""
        response = self.client.post('/api/users/sjdf63j/times/', {'date': '2014-07-07', 'duration': '1:32:00', 'distance': 11.3}, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.get('/api/users/sjdf63j/weeks/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, [date(2014, 7, 7)])

    def test_weeks_no_credentials_403(self):
        """Returns 403 if no credentials are present."""
        self.client.force_authenticate()
        response = self.client.get('/api/users/sjdf63j/weeks/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['detail'], "Authentication credentials were not provided.")

    def test_weeks_invalid_credentials_403(self):
        """Returns 403 if invalid credentials are present."""
        self.client.force_authenticate()
        self.client.credentials(HTTP_AUTHORIZATION='JWT thisIsNotAValidJWTToken')
        response = self.client.get('/api/users/power_runner/weeks/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['detail'], "Error decoding signature.")

    def test_weeks_wrong_credentials_403(self):
        """Returns 403 if wrong credentials are present."""
        response = self.client.get('/api/users/power_runner/weeks/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['detail'], "Can't access other users' data.")


class SpecificWeekTestCase(APITestCase):
    def setUp(self):
        user = User.objects.create_user(
            NEW_TEST_USER,
            '',
            NEW_TEST_PASS,
        )
        self.client.force_authenticate(user=user)

    def test_specific_week_no_credentials_403(self):
        """Returns 403 if no credentials are present for a specific week."""
        self.client.force_authenticate()
        response = self.client.get('/api/users/sjdf63j/weeks/2014-06-30/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['detail'], "Authentication credentials were not provided.")

    def test_specific_week_invalid_credentials_403(self):
        """Returns 403 if invalid credentials are present."""
        self.client.force_authenticate()
        self.client.credentials(HTTP_AUTHORIZATION='JWT thisIsNotAValidJWTToken')
        response = self.client.get('/api/users/power_runner/weeks/2014-06-30/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['detail'], "Error decoding signature.")

    def test_specific_week_wrong_credentials_403(self):
        """Returns 403 if wrong credentials are present for a specific week."""
        response = self.client.get('/api/users/power_runner/weeks/2014-06-30/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['detail'], "Can't access other users' data.")

    @patch('server.views.date', SpoofDate)
    def test_specific_week_with_one_entry(self):
        """Creating a new entry for today and then weeks return this monday"""
        response = self.client.post('/api/users/sjdf63j/times/', {'date': '2014-07-02', 'duration': '1:32:00', 'distance': 11.3}, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.get('/api/users/sjdf63j/weeks/2014-06-30/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        entry = response.data[0]
        self.assertEqual(entry['username'], NEW_TEST_USER)
        self.assertEqual(entry['date'], date(2014, 7, 2))
        self.assertEqual(entry['duration'], '1:32:00')
        self.assertEqual(entry['distance'], Decimal('11.3'))


class TimesTestCase(APITestCase):
    def setUp(self):
        user = User.objects.get(username='power_runner')
        self.client.force_authenticate(user=user)

    def test_times_no_credentials_403(self):
        """Returns 403 if no credentials are present for times."""
        self.client.force_authenticate()
        response = self.client.get('/api/users/power_runner/times/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['detail'], "Authentication credentials were not provided.")

    def test_times_invalid_credentials_403(self):
        """Returns 403 if no credentials are present for times."""
        self.client.force_authenticate()
        self.client.credentials(HTTP_AUTHORIZATION='JWT thisIsNotAValidJWTToken')
        response = self.client.get('/api/users/power_runner/times/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['detail'], "Error decoding signature.")

    def test_times_wrong_credentials_403(self):
        """Returns 403 if wrong credentials are present for times."""
        response = self.client.get('/api/users/casual_runner/times/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['detail'], "Can't access other users' data.")

    def test_times_no_filter(self):
        """Can retrieve times with no filter"""
        response = self.client.get('/api/users/power_runner/times/')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 6)

    def test_can_filter_by_start_date(self):
        """Can filter by start date"""
        response = self.client.get('/api/users/power_runner/times/?from=2014-06-24')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)

    def test_can_filter_by_end_date(self):
        """Can filter by end date"""
        response = self.client.get('/api/users/power_runner/times/?to=2014-06-24')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 5)

    def test_can_filter_by_start_and_end_date(self):
        """Can filter by start and end dates"""
        response = self.client.get('/api/users/power_runner/times/?from=2014-06-23&to=2014-06-24')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)

    def test_cannot_create_time_for_another_user(self):
        """Can filter by start and end dates"""
        response = self.client.post('/api/users/casual_runner/times/', {'date': '2014-07-07', 'duration': '1:32:00', 'distance': 11.3}, format='json')

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['detail'], "Can't access other users' data.")


class SpecificTimeTestCase(APITestCase):
    def setUp(self):
        user = User.objects.get(username='power_runner')
        self.client.force_authenticate(user=user)

    def test_specific_time_no_credentials_403(self):
        """Returns 403 if no credentials are present for a specific time."""
        self.client.force_authenticate()
        response = self.client.get('/api/users/power_runner/times/6/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['detail'], "Authentication credentials were not provided.")

    def test_specific_time_invalid_credentials_403(self):
        """Returns 403 if no credentials are present for a specific time."""
        self.client.force_authenticate()
        self.client.credentials(HTTP_AUTHORIZATION='JWT thisIsNotAValidJWTToken')
        response = self.client.get('/api/users/power_runner/times/6/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['detail'], "Error decoding signature.")

    def test_specific_time_wrong_credentials_403(self):
        """Returns 403 if wrong credentials are present for a specific time."""
        response = self.client.get('/api/users/casual_runner/times/4/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['detail'], "Can't access other users' data.")

    def test_specific_time_wrong_related_user_404(self):
        """Returns 404 if a wrong username is given for a specific time."""
        response = self.client.get('/api/users/power_runner/times/4/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(response.data['detail'], "Not found")

    def test_retrieve_a_specific_time(self):
        """Retrieving a specific time and its properties"""
        response = self.client.get('/api/users/power_runner/times/6/')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['username'], 'power_runner')
        self.assertEqual(response.data['duration'], '0:57:00')
        self.assertEqual(response.data['date'], date(2014, 6, 22))
        self.assertEqual(response.data['distance'], Decimal('12'))

    def test_can_delete_time(self):
        """Can delete a time"""
        response = self.client.delete('/api/users/power_runner/times/6/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        response = self.client.get('/api/users/power_runner/times/6/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class AuthenticationTestCase(APITestCase):
    def test_create_a_user(self):
        """A user can be created via POST to /api/register/"""
        response = self.client.post('/api/register/', {'username': NEW_TEST_USER, 'password': NEW_TEST_PASS}, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        user = User.objects.get(username=NEW_TEST_USER)
        self.assertEqual(user.username, NEW_TEST_USER)

    def test_cannot_register_existing_user(self):
        """Returns 400 if trying to register an existing user."""
        response = self.client.post('/api/register/', {'username': 'power_runner', 'password': NEW_TEST_PASS}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['username'][0], "User with this Username already exists.")

    def test_cannot_create_with_empty_password(self):
        """Returns 400 if trying to register user with empty password."""
        response = self.client.post('/api/register/', {'username': NEW_TEST_USER, 'password': ''}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['password'][0], "Password can't be empty.")

    def test_register_must_provide_username(self):
        """Returns 400 if trying to register user with empty username."""
        response = self.client.post('/api/register/', {'password': NEW_TEST_PASS}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['username'][0], "This field is required.")

    def test_register_must_provide_password(self):
        """Returns 400 if trying to register user with empty password."""
        response = self.client.post('/api/register/', {'username': NEW_TEST_USER}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['password'][0], "This field is required.")

    @patch('rest_framework_jwt.utils.datetime.datetime', SpoofDateTime)
    def test_authenticate_a_user(self):
        """A user can be authenticated via POST to /api/authenticate/."""
        response = self.client.post('/api/authenticate/', {'username': 'power_runner', 'password': 'toptal'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('token', response.data)

    def test_authenticate_must_provide_username(self):
        """Returns 400 if trying to authenticate user with empty username."""
        response = self.client.post('/api/authenticate/', {'password': NEW_TEST_PASS}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['username'][0], "This field is required.")

    def test_authenticate_must_provide_password(self):
        """Returns 400 if trying to authenticate user with empty password."""
        response = self.client.post('/api/authenticate/', {'username': NEW_TEST_USER}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['password'][0], "This field is required.")
