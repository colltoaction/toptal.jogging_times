from django.contrib.auth.models import User
from rest_framework import serializers
from server.models import Time


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'times')

class TimeSerializer(serializers.ModelSerializer):
    username = serializers.CharField(source='user.username', required=False)

    class Meta:
        model = Time
        fields = ('id', 'user', 'username', 'date', 'distance', 'duration')
