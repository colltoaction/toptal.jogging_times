from datetime import date, timedelta
from django.contrib.auth.models import User
from django.db import models
from durationfield.db.models.fields.duration import DurationField


class Time(models.Model):
    user = models.ForeignKey(User, related_name="times")
    date = models.DateField(default=date.today())
    distance = models.DecimalField(max_digits=5, decimal_places=2)
    duration = DurationField()
