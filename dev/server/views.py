from datetime import date, datetime, timedelta
from math import ceil
from django.db.models import Min
from django.contrib.auth.models import User
from rest_framework import status, viewsets
from rest_framework.exceptions import PermissionDenied
from rest_framework.parsers import JSONParser
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.decorators import api_view, parser_classes, permission_classes
from server import serializers
from server.models import Time


class UserViewSet(viewsets.ViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer

@api_view(['POST'])
@permission_classes((AllowAny,))
def register_user(request, **kwargs):
    data = JSONParser().parse(request)
    serialized = serializers.UserSerializer(data=data)

    if serialized.is_valid():
        if 'password' not in serialized.init_data:
            return Response({'password': ["This field is required."]}, status=status.HTTP_400_BAD_REQUEST)
        if serialized.init_data['password'] == "":
            return Response({'password': ["Password can't be empty."]}, status=status.HTTP_400_BAD_REQUEST)

        user = User.objects.create_user(
            serialized.init_data['username'],
            '', #serialized.init_data['email'],
            serialized.init_data['password'],
        )

        serialized_user = serializers.UserSerializer(user)
        return Response(serialized_user.data, status=status.HTTP_201_CREATED)
    else:
        return Response(serialized._errors, status=status.HTTP_400_BAD_REQUEST)

class TimeViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows time entries to be viewed or edited.
    """
    queryset = Time.objects.all()
    serializer_class = serializers.TimeSerializer

    def get_queryset(self):
        if self.request.user.username != self.kwargs['user_pk']:
            raise PermissionDenied(detail="Can't access other users' data.")
        queryset = Time.objects.filter(user__username=self.request.user.username)
        fromDate = self.request.QUERY_PARAMS.get('from', None)
        if fromDate is not None:
            queryset = queryset.filter(date__gte=fromDate)
        toDate = self.request.QUERY_PARAMS.get('to', None)
        if toDate is not None:
            queryset = queryset.filter(date__lte=toDate)
        return queryset.order_by('-date')

    def create(self, request, **kwargs):
        if request.user.username != self.kwargs['user_pk']:
            raise PermissionDenied(detail="Can't access other users' data.")
        request.DATA['user'] = request.user.id
        return super(TimeViewSet, self).create(request, **kwargs)

class WeeklyTimeViewSet(viewsets.ViewSet):
    """
    API endpoint that exposes a readonly preprocessed weekly view of the time entries.
    """
    def get_queryset(self):
        if self.request.user.username != self.kwargs['user_pk']:
            raise PermissionDenied(detail="Can't access other users' data.")
        return Time.objects.filter(user__username=self.request.user.username)

    def retrieve(self, request, user_pk, pk=None):
        start_of_week = datetime.strptime(pk, "%Y-%m-%d")
        end_of_week = start_of_week + timedelta(weeks=1)
        queryset = self.get_queryset().filter(date__gte=start_of_week).filter(date__lt=end_of_week)
        serializer = serializers.TimeSerializer(queryset, many=True)
        return Response(serializer.data)

    def list(self, request, user_pk):
        queryset = self.get_queryset()
        min_date = queryset.aggregate(Min('date'))['date__min']
        if min_date is None:
            return Response([])
            
        start_of_week = min_date - timedelta(days=min_date.weekday())
        end_of_prev_week = start_of_week - timedelta(days=1)
        no_weeks_ago = ceil((date.today() - end_of_prev_week) / timedelta(days=7))
        date_list = [start_of_week + timedelta(weeks=x) for x in range(no_weeks_ago)]
        return Response(date_list)
