angular.module('app', ['ngRoute', 'djangoRESTResources'])

.factory('Entries', function(djResource) {
  return djResource("api/entries/:id", {});
})

.factory('Users', function(djResource) {
  return djResource("api/users/:id", {});
})

.config(function($routeProvider, $locationProvider) {
  $locationProvider.html5Mode(true);
  $routeProvider
  .when('/entries', {
    controller:'EntriesCtrl',
    templateUrl:'templates/entries.html'
  })
  .when('/users', {
    controller:'UsersCtrl',
    templateUrl:'templates/users.html'
  })
  .otherwise({
    redirectTo:'/entries'
  });
})

.controller('EntriesCtrl', function($scope, Entries) {
  Entries.get(function(data){
    $scope.entries = data.results;
  });
})

.controller('UsersCtrl', function($scope, Users) {
  Users.get(function(data){
    $scope.users = data.results;
  });
});