from django.shortcuts import render_to_response
from spike1.api import UserResource


def user_detail(request, username):
    ur = UserResource()
    ur_bundle = ur.build_bundle(request=request)
    user = ur.obj_get(bundle = ur_bundle, username=username)

    # Other things get prepped to go into the context then...

    return render_to_response('user_detail.html', {
        # Other things here.
        "user_json": ur.serialize(None, ur.full_dehydrate(ur_bundle), 'application/json'),
    })