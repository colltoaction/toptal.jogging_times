from django.contrib.auth.models import User
from tastypie import fields
from tastypie.resources import ModelResource
from spike1.models import Entry


class UserResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        fields = ['username', 'first_name', 'last_name', 'last_login']
        allowed_methods = ['get']


class EntryResource(ModelResource):
    user = fields.ForeignKey(UserResource, 'user')

    class Meta:
        queryset = Entry.objects.all()